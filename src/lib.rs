extern crate db_sys;
#[macro_use(bitflags)]
extern crate bitflags;
extern crate libc;

use std::ptr::{null, null_mut};
use std::ffi::CString;
use std::str;
use std::slice;
use std::mem::transmute;

pub use db_sys::DB_CREATE;
pub use db_sys::DB_RDONLY;

pub use db_sys::DBTYPE;

pub use db_sys::DB_KEYFIRST;
pub use db_sys::DB_CURRENT;
pub use db_sys::DB_NEXT;

pub struct Db(*mut db_sys::DB);
pub struct DbEnv(*mut db_sys::DB_ENV);
pub struct DbTxn(*mut db_sys::DB_TXN);
pub struct Dbt(db_sys::DBT);
pub struct Dbc(*mut db_sys::DBC);

#[derive(Debug, PartialEq)]
pub enum Error {
    NotFound,
    Unknown(i32),
}

impl Db {
    pub fn new(dbenv: Option<DbEnv>, flags: u32) -> Result<Db, Error> {
        let mut db = null_mut();
        let error = unsafe {
            db_sys::db_create(&mut db as *mut *mut db_sys::DB,
                              match dbenv {
                                  Some(DbEnv(dbenv)) => dbenv,
                                  None => null_mut(),
                              },
                              flags)
        };
        match error {
            0 => Ok(Db(db)),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn open(&self,
                txn: Option<DbTxn>,
                file: &str,
                database: Option<&str>,
                dbtype: DBTYPE,
                flags: u32,
                mode: i32)
                -> Result<(), Error> {
        let txn = match txn {
            Some(DbTxn(txn)) => txn,
            None => null_mut(),
        };
        let file = CString::new(file).unwrap();
        let database = match database {
            Some(s) => {
                let cs = CString::new(s).unwrap();
                cs.as_ptr()
            }
            None => null(),
        };
        let error = match self {
            &Db(db) => unsafe {
                ((*db).open)(db, txn, file.as_ptr(), database, dbtype, flags, mode)
            },
        };
        match error {
            0 => Ok(()),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn close(&self, flags: u32) -> Result<(), Error> {
        let error = match self {
            &Db(db) => unsafe { ((*db).close)(db, flags) },
        };
        match error {
            0 => Ok(()),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn put(&self, txn: Option<DbTxn>, key: &Dbt, data: &Dbt, flags: u32) -> Result<(), Error> {
        let txn = match txn {
            Some(DbTxn(txn)) => txn,
            None => null_mut(),
        };
        let key = match key {
            &Dbt(ref key) => unsafe { transmute(key) },
        };
        let data = match data {
            &Dbt(ref data) => unsafe { transmute(data) },
        };
        let error = match self {
            &Db(db) => unsafe { ((*db).put)(db, txn, key, data, flags) },
        };
        match error {
            0 => Ok(()),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn get(&self,
               txn: Option<DbTxn>,
               key: &Dbt,
               data: &mut Dbt,
               flags: u32)
               -> Result<(), Error> {
        let txn = match txn {
            Some(DbTxn(txn)) => txn,
            None => null_mut(),
        };
        let key = match key {
            &Dbt(ref key) => unsafe { transmute(key) },
        };
        let data = match data {
            &mut Dbt(ref mut data) => data as *mut db_sys::DBT,
        };
        let error = match self {
            &Db(db) => unsafe { ((*db).get)(db, txn, key, data, flags) },
        };
        match error {
            0 => Ok(()),
            db_sys::DB_NOTFOUND => Err(Error::NotFound),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn del(&self, txn: Option<DbTxn>, key: &Dbt, flags: u32) -> Result<(), Error> {
        let txn = match txn {
            Some(DbTxn(txn)) => txn,
            None => null_mut(),
        };
        let key = match key {
            &Dbt(ref key) => unsafe { transmute(key) },
        };
        let error = match self {
            &Db(db) => unsafe { ((*db).del)(db, txn, key, flags) },
        };
        match error {
            0 => Ok(()),
            db_sys::DB_NOTFOUND => Err(Error::NotFound),
            code => Err(Error::Unknown(code)),
        }
    }
}

impl Dbt {
    pub fn new() -> Dbt {
        Dbt(db_sys::DBT {
            data: null_mut(),
            size: 0,
            ulen: 0,
            dlen: 0,
            doff: 0,
            flags: 0,
        })
    }

    pub fn as_bytes(&self) -> &[u8] {
        match self {
            &Dbt(ref dbt) => unsafe {
                slice::from_raw_parts_mut(dbt.data as *mut u8, dbt.size as usize)
            },
        }
    }
}

impl<'a> From<&'a [u8]> for Dbt {
    fn from(data: &[u8]) -> Dbt {
        Dbt(db_sys::DBT {
            data: data.as_ptr() as *mut libc::c_void,
            size: data.len() as u32,
            ulen: 0,
            dlen: 0,
            doff: 0,
            flags: 0,
        })
    }
}

impl<'a> From<&'a str> for Dbt {
    fn from(s: &'a str) -> Dbt {
        Dbt::from(s.as_bytes())
    }
}

impl<'a> From<&'a Dbt> for String {
    fn from(dbt: &'a Dbt) -> String {
        str::from_utf8(dbt.as_bytes())
            .unwrap()
            .to_string()
    }
}

impl Dbc {
    pub fn new(db: &Db, txn: Option<DbTxn>, flags: u32) -> Result<Dbc, Error> {
        let txn = match txn {
            Some(DbTxn(txn)) => txn,
            None => null_mut(),
        };
        let mut cursorp = null_mut();
        let error = match db {
            &Db(db) => unsafe {
                ((*db).cursor)(db, txn, &mut cursorp as *mut *mut db_sys::DBC, flags)
            },
        };
        match error {
            0 => Ok(Dbc(cursorp)),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn close(&self) -> Result<(), Error> {
        let error = match self {
            &Dbc(cursorp) => unsafe { ((*cursorp).c_close)(cursorp) },
        };
        match error {
            0 => Ok(()),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn get(&self, key: &mut Dbt, data: &mut Dbt, flags: u32) -> Result<(), Error> {
        let key = match key {
            &mut Dbt(ref mut key) => key as *mut db_sys::DBT,
        };
        let data = match data {
            &mut Dbt(ref mut data) => data as *mut db_sys::DBT,
        };
        let error = match self {
            &Dbc(cursorp) => unsafe { ((*cursorp).c_get)(cursorp, key, data, flags) },
        };
        match error {
            0 => Ok(()),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn put(&self, key: &Dbt, data: &Dbt, flags: u32) -> Result<(), Error> {
        let key = match key {
            &Dbt(ref key) => unsafe { transmute(key) },
        };
        let data = match data {
            &Dbt(ref data) => unsafe { transmute(data) },
        };
        let error = match self {
            &Dbc(cursorp) => unsafe { ((*cursorp).c_put)(cursorp, key, data, flags) },
        };
        match error {
            0 => Ok(()),
            db_sys::DB_NOTFOUND => Err(Error::NotFound),
            code => Err(Error::Unknown(code)),
        }
    }

    pub fn del(&self, flags: u32) -> Result<(), Error> {
        let error = match self {
            &Dbc(cursorp) => unsafe { ((*cursorp).c_del)(cursorp, flags) },
        };
        match error {
            0 => Ok(()),
            db_sys::DB_NOTFOUND => Err(Error::NotFound),
            code => Err(Error::Unknown(code)),
        }
    }
}

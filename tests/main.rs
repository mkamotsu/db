extern crate db;

use db::*;

#[test]
fn create() {
    Db::new(None, 0).unwrap();
}

#[test]
fn open_and_close() {
    let db = Db::new(None, 0).unwrap();
    db.open(None,
              "test_open_and_close.db",
              None,
              DBTYPE::DB_HASH,
              DB_CREATE,
              0)
        .unwrap();
    db.close(0).unwrap();
    std::fs::remove_file("test_open_and_close.db").unwrap();
}

#[test]
fn put() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_put.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("aaa"), &mut Dbt::from("bbb"), 0).unwrap();
    db.close(0).unwrap();
    std::fs::remove_file("test_put.db").unwrap();
}

#[test]
fn get() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_get.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("ccc"), &Dbt::from("ddd"), 0).unwrap();
    let mut data = Dbt::new();
    db.get(None, &Dbt::from("ccc"), &mut data, 0).unwrap();
    assert_eq!(String::from(&data), "ddd");
    db.close(0).unwrap();
    std::fs::remove_file("test_get.db").unwrap();
}

#[test]
fn del() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_del.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("eee"), &Dbt::from("fff"), 0).unwrap();
    let mut data = Dbt::new();
    db.get(None, &Dbt::from("eee"), &mut data, 0).unwrap();
    assert_eq!(String::from(&data), "fff");
    db.del(None, &Dbt::from("eee"), 0).unwrap();
    let mut data = Dbt::new();
    assert_eq!(db.get(None, &Dbt::from("eee"), &mut data, 0),
               Err(Error::NotFound));
    db.close(0).unwrap();
    std::fs::remove_file("test_del.db").unwrap();
}

#[test]
fn dbt() {
    assert_eq!(String::from(&Dbt::from("ggg")), "ggg");
    assert_eq!(String::from(&Dbt::from("hhh")), "hhh");
}

#[test]
fn dbc() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_dbc.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("iii"), &Dbt::from("jjj"), 0).unwrap();
    let mut data = Dbt::new();
    db.get(None, &Dbt::from("iii"), &mut data, 0).unwrap();
    assert_eq!(String::from(&data), "jjj");
    Dbc::new(&db, None, 0).unwrap();
    db.close(0).unwrap();
    std::fs::remove_file("test_dbc.db").unwrap();
}

#[test]
fn c_close() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_c_close.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("kkk"), &Dbt::from("lll"), 0).unwrap();
    let mut data = Dbt::new();
    db.get(None, &Dbt::from("kkk"), &mut data, 0).unwrap();
    assert_eq!(String::from(&data), "lll");
    let dbc = Dbc::new(&db, None, 0).unwrap();
    dbc.close().unwrap();
    db.close(0).unwrap();
    std::fs::remove_file("test_c_close.db").unwrap();
}

#[test]
fn c_get() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_c_get.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("nnn"), &Dbt::from("mmm"), 0).unwrap();
    let dbc = Dbc::new(&db, None, 0).unwrap();
    let mut key = Dbt::new();
    let mut data = Dbt::new();
    dbc.get(&mut key, &mut data, DB_NEXT).unwrap();
    assert_eq!(String::from(&key), "nnn");
    assert_eq!(String::from(&data), "mmm");
    dbc.close().unwrap();
    db.close(0).unwrap();
    std::fs::remove_file("test_c_get.db").unwrap();
}

#[test]
fn c_put() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_c_put.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    let dbc = Dbc::new(&db, None, 0).unwrap();
    dbc.put(&Dbt::from("ooo"), &Dbt::from("ppp"), DB_KEYFIRST).unwrap();
    dbc.close().unwrap();
    let mut data = Dbt::new();
    db.get(None, &Dbt::from("ooo"), &mut data, 0).unwrap();
    assert_eq!(String::from(&data), "ppp");
    db.close(0).unwrap();
    std::fs::remove_file("test_c_put.db").unwrap();
}

#[test]
fn c_del() {
    let db = Db::new(None, 0).unwrap();
    db.open(None, "test_c_del.db", None, DBTYPE::DB_HASH, DB_CREATE, 0).unwrap();
    db.put(None, &Dbt::from("qqq"), &Dbt::from("rrr"), 0).unwrap();
    let dbc = Dbc::new(&db, None, 0).unwrap();
    dbc.get(&mut Dbt::new(), &mut Dbt::new(), DB_NEXT).unwrap();
    dbc.del(0).unwrap();
    dbc.close().unwrap();
    assert_eq!(db.get(None, &Dbt::from("qqq"), &mut Dbt::new(), 0),
               Err(Error::NotFound));
    db.close(0).unwrap();
    std::fs::remove_file("test_c_del.db").unwrap();
}
